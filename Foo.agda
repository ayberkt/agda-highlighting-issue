module Foo where

open import Relation.Binary.PropositionalEquality using    (_≡_; refl)
                                                  renaming (cong to ap)
open import Data.Nat                              using    (ℕ; suc; zero)

-- The addition function.
_+_ : ℕ → ℕ → ℕ
zero  + n = n
suc m + n = suc (m + n)

-- 0 is the right identity for addition.
+-right-id : (n : ℕ) → n + zero ≡ n
+-right-id zero    = refl
+-right-id (suc n) = ap suc (+-right-id n)
